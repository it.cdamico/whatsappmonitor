import psycopg2

def get_connection():
    hostname = "db-postgresql-nyc3-50653-do-user-10428222-0.b.db.ondigitalocean.com"
    port = 25060
    username = "doadmin"
    password = "OLeMPcq39FMxaNf5"
    database = "defaultdb"

    return psycopg2.connect(
                host=hostname,
                port=port,
                database=database,
                user=username,
                password=password)

def clean():
    try:
        conn = get_connection()
        cur = conn.cursor()
        commands = ['TRUNCATE TABLE TABLE_0;','TRUNCATE TABLE TABLE_1;','TRUNCATE TABLE MATCHES;']
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def setup():
    try:
        conn = get_connection()
        cur = conn.cursor()

        commands = [
            """CREATE TABLE IF NOT EXISTS TABLE_0 (
                        message_id SERIAL PRIMARY KEY,
                        sender VARCHAR(255) NOT NULL,
                        date VARCHAR(255) NOT NULL,
                        content VARCHAR(255) NOT NULL
                        )""",
            """CREATE TABLE IF NOT EXISTS TABLE_1 (
                        message_id SERIAL PRIMARY KEY,
                        sender VARCHAR(255) NOT NULL,
                        date VARCHAR(255) NOT NULL,
                        content VARCHAR(255) NOT NULL
                        )""",
            """CREATE TABLE IF NOT EXISTS MATCHES (
                        message_id SERIAL PRIMARY KEY,
                        recipient VARCHAR(255) NOT NULL,
                        date VARCHAR(255) NOT NULL,
                        content VARCHAR(255) NOT NULL
                        )"""
        ]
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def query(table, last_id=-1):

    results = []

    try:
        conn = get_connection()
        cur = conn.cursor()
        cur.execute(f"SELECT * FROM {table} WHERE message_id > {last_id}")
        row = cur.fetchone()

        while row is not None:
            results.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return results


def insert_to(table, sender, date, content, is_test= False):
    sql = f"""INSERT INTO {table}(sender,date,content)
             VALUES(%s,%s,%s) RETURNING message_id;"""
    message_id = None
    try:
        conn = get_connection()
        cur = conn.cursor()
        if not is_test:
            cur.execute(sql, (sender, date, content))
        else:
            cur.execute(sql, (sender[:2], date, f"A{content[:2]} B{content[:2]} C{content[:2]}"))
        message_id = cur.fetchone()[0]
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return message_id


def matched(recipient, date, content, is_test=False):
    sql = """INSERT INTO MATCHES(recipient,date,content)
             VALUES(%s,%s,%s) RETURNING message_id;"""
    match_id = None
    try:
        conn = get_connection()
        cur = conn.cursor()
        if not is_test:
            cur.execute(sql, (recipient, date, content))
        else:
            cur.execute(sql, (recipient[:2], date, f"A{content[:2]} B{content[:2]} C{content[:2]}"))
        match_id = cur.fetchone()[0]
        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return match_id

