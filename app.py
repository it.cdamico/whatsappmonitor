from db import query
import pandas as pd
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('tables.html', title='Whatsapp Monitor ')

last_t0 = last_t1 = last_m = -1

@app.route('/api/table_0')
def t0():
    global last_t0
    new_data = pd.DataFrame(query("TABLE_0",last_t0),columns=["message_id","sender","date","content"])
    new_data["message_id"] = new_data["message_id"].max() - new_data["message_id"]
    #last_t0 = new_data["message_id"].max()
    return {'data': new_data.to_dict(orient="records")}

@app.route('/api/table_1')
def t1():
    global last_t1
    new_data = pd.DataFrame(query("TABLE_1",last_t1),columns=["message_id","sender","date","content"])
    new_data["message_id"] = new_data["message_id"].max() - new_data["message_id"]
    #last_t1 = new_data["message_id"].max()
    return {'data': new_data.to_dict(orient="records")}


@app.route('/api/matched')
def t2():
    global last_m
    new_data = pd.DataFrame(query("MATCHES",last_m),columns=["message_id","recipient","date","content"])
    new_data["message_id"] = new_data["message_id"].max() - new_data["message_id"]
    #last_m = new_data["message_id"].max()
    return {'data': new_data.to_dict(orient="records")}

if __name__ == "__main__":
    app.run(port=5051)